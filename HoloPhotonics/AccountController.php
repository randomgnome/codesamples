<?php

namespace App\Http\Controllers;

use DateTime;
use App\Models\Account;
use App\Models\Product;
use App\Models\SystemSetting;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AccountController extends Controller
{

    public function returnOK() {
        return response('OK', 200)->json();
    }

    public function fetch($id) {
        // Fetch Account
        $account = Account::find($id);

        // Validate
        if ($account == null) throw new ValidationException('Invalid Account Id');

        // Return
        return response()->json(['success' => true, 'data' => ['account' => $account]]);
    }

    public function fetchByEmail(Request $request) {
        // Validate Data
        $this->validate($request, ['email' => 'required'], ['required' => ':attribute is required']);
        
        // Fetch Data from Request
        $email = $request->input('email');
        $partial = $request->input('partial', false);

        // Fetch and return account
        if ($partial) $account = Account::where('email', 'regexp', '/' . $email . '/i')->get();
        else $account = Account::where('email', '=', strtoupper($email))->first();

        if (!$account) throw new ValidationException('No Results');
        
        return response()->json(['success' => true, 'data' => ['account' => $account]]);
    }

    public function fetchByUUID(Request $request) {
        // Validate Data
        $this->validate($request, ['uuid' => 'required'], ['required' => ':attribute is required']);
        
        // Fetch Data From Request
        $uuid = $request->input('uuid');
        
        // Fetch and return account
        $account = Account::where('devices.uuid', '=', $uuid)->first();
        if (!$account) throw new ValidationException('No Results');
        return response()->json(['success' => true, 'data' => ['account' => $account]]);
        
    }

    public function stats() {
        // Determine Stats
        $totalAccounts = Account::count();
        $active = Account::where('active', true)->count();
        $trial = 0;

        foreach (Account::where('active', false)->get() as $trialAcct) {
            $now = new DateTime();
            $created = new DateTime($trialAcct->created_at);

            if ($now->diff($created)->format('%a') < $trialAcct->trialPeriod) $trial++;
        }

        // Return
        return response()->json(['success' => true, 'data' => ['total' => $totalAccounts, 'active' => $active, 'trial' => $trial]]);
    }

    public function paginate(Request $request) {
        // Fetch Data From Request
        $limit = (int)$request->input('limit', 15);
        $page = (int)$request->input('page', 1);

        // Get Total Accounts
        $total = Account::count();

        // Get Account Page
        $accounts = Account::skip(($page - 1) * $limit)->take($limit)->get();

        // Return
        return response()->json(['success' => true, 'data' => ['accounts' => $accounts, 'total' => $total, 'page' => $page, 'limit' => $limit]]);
    }

    public function update($id, Request $request) {
        // Check for existing account
        $account = Account::find($id);

        // If there is none, throw exception
        if ($account == null) throw new ValidationException('Could not find account');

        $this->validate($request, [
            'trialPeriod' => 'numeric',
            'maxDevices' => 'numeric',
            'email' => 'email'
        ], [
            'numeric' => ':attribute must be numeric',
            'email' => ':attribute must be a valid email'
        ]);

        // Fetch Data from Request
        $email = $request->input('email');
        $first = $request->input('first');
        $last = $request->input('last');
        $active = $request->input('active');
        $trialPeriod = (int)$request->input('trialPeriod');
        $maxDevices = (int)$request->input('maxDevices');

        // Store On Account
        if ($email && $account->email != strtoupper($email)) {
            if (Account::where('email', '=', strtoupper($email))->first() != null) throw new ValidationException('That email is already in use');
            $account->email = strtoupper($email);
        }

        if ($first) $account->first = strtoupper($first);
        if ($last) $account->last = strtoupper($last);
        if ($trialPeriod) $account->trialPeriod = $trialPeriod;
        if ($maxDevices) $account->maxDevices = $maxDevices;

        if ($active) {
            $account->active = true;
            $account->activation = now();
        } else {
            $account->active = false;
            $account->activation = null;
        }
        
        // Save
        $account->save();
        
        // Response
        return response()->json(['success' => true, 'data' => ['account' => $account]]);
    }

    public function addDevice($id, Request $request) {
        // Validate Data
        $this->validate($request, ['uuid' => 'required'], ['required' => ':attribute is required']);
        
        // Fetch Data from Request
        $uuid = $request->input('uuid');

        // Check for existing account
        $account = Account::find($id);

        // If there is none, throw exception
        if ($account == null) throw new ValidationException('Could not find account');

        // Add device to Account and update Account
        $account->addDevice($uuid, true);

        return response()->json(['success' => true, 'data' => ['account' => $account]]);
    }

    public function productSync(Request $request) {
        if (!SystemSetting::has('inusionToken')) throw new ValidationException('Infusionsoft not setup');

        // Validate Data
        $this->validate($request, ['email' => 'required|email'], ['required' => ':attribute is required']);

        // Fetch Data from Request
        $email = $request->input('email');

        // Find Product
        $account = Account::where('email', '=', strtoupper($email))->first();
        if ($account == null) throw new ValidationException('Could not find account');
        
        // Ensure we're in Infusion
        if (!$account->infusionId) $account->infusionSync($request->input('infusion', null));

        // Fetch Infusion API & Infusion Data
        $infusion = infusion();
        $invoices = $infusion->data()->query('Invoice', 500, 0, ['ContactId' => $account->infusionId], ['TotalDue', 'TotalPaid', 'RefundStatus', 'ProductSold'], 'Id', false);

        if (is_array($invoices) && count($invoices)) {
            $success = [];
            $failure = [];
            foreach ($invoices as $invoice) {
                if ($invoice['TotalPaid'] >= $invoice['TotalDue'] && strlen($invoice['ProductSold'])) {
                    foreach (explode(',', $invoice['ProductSold']) as $productId) {
                        $product = Product::findByInfusionId($productId);
                        if ($product) {
                            $account->addProduct($product->sku, true);
                            $success[] = $product->name;
                        } else $failure[] = $productId;
                    }
                }
            }
            return response()->json(['success' => true, 'data' => ['invoices' => $invoices, 'added' => $success, 'failed' => $failure]]);
        }

        return response()->json(['success' => false, 'data' => ['invoices' => $invoices]]);
    }

    public function addProduct($id, Request $request) {
        // Validate Data
        $this->validate($request, ['sku' => 'required'], ['required' => ':attribute is required']);
        
        // Fetch Data from Request
        $sku = $request->input('sku');

        // Check for existing account
        $account = Account::find($id);

        // If there is none, throw exception
        if ($account == null) throw new ValidationException('Could not find account');

        // Add device to Account and update Account
        $account->addProduct($sku, true);

        return response()->json(['success' => true, 'data' => ['account' => $account]]);
    }

    public function coldActivate(Request $request) {
        $account = null;

        // If ID is provided, try to fetch the associated account
        if (($id = $request->input('id', false)) !== FALSE)
            $account = Account::find($id);

        // If we don't have an account but we do have an email, try to find an existing account via email
        if ($account == null && ($email = $request->input('email', false)) !== FALSE)
            $account = Account::where('email', '=', strtoupper($email))->first();
        
        // If Account is not valid by this point, try to create it
        if (!$account) 
            $account = $this->processRegistration($request);

        // Try to activate account
        $account = $this->processActivation($account->id, $request);
        
        // Response
        return response()->json(['success' => true, 'data' => ['account' => $account]]);
    }

    public function activate($id, Request $request) {            
        $account = $this->processActivation($id, $request);
        return response()->json(['success' => true, 'data' => ['account' => $account]]);
    }

    public function register(Request $request) {           
        $account = $this->processRegistration($request);
        return response()->json(['success' => true, 'data' => ['account' => $account]]);
    }

    private function processRegistration(Request $request) {
        // Validate Data
        $this->validate($request, ['email' => 'required|email', 'first' => 'required', 'last' => 'required'], [
            'required' => ':attribute is required',
            'email' => ':attribute must be a valid email'
        ]);

        // Fetch Data from Request
        $email = $request->input('email');
        $first = $request->input('first');
        $last = $request->input('last');
        $InfusionContactID = $request->input('infusionId', false);
            
        // Check for existing account
        $existingAccount = Account::where('email', '=', strtoupper($email))->first();
        if ($existingAccount != null) throw new ValidationException('That email is already in use');

        // Create new account
        $account = new Account();
        $account->first = strtoupper($first);
        $account->last = strtoupper($last);
        $account->email = strtoupper($email);

        // Save Account
        $account->save();
        
        // Sync to Infusionsoft
        $account->infusionSync($InfusionContactID);

        return $account;
    }

    private function processActivation($id, Request $request) {
        // Fetch Data from Request
        $invoice = $request->input('invoice');
            
        // Check for existing account
        $account = Account::find($id);

        // If there is none, throw exception
        if ($account == null) throw new ValidationException('Could not find account');
        if ($account->active) throw new ValidationException('Account is already active');

        // Set Active Flag
        $account->active = true;
        $account->activation = now();
        
        // Save Invoice ID
        if ($invoice) $account->invoice = $invoice;

        // Add "Auto Add" products to newly activated account
        foreach (Product::where('autoAdd', '=', true)->get() as $product) {
            $account->addProduct($product->sku);
        }
            
        // Save Account
        $account->save();

        return $account;
    }

}
