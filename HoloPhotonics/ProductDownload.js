angular.module('holophotonic')
    .controller('ProductDownloadCtrl', function($scope, $state, $q, ProductHelper, missingProducts, _) {

        $scope.showDownload = false;
        $scope.download = function() {
            $scope.showDownload = true;

            _.reduce(missingProducts, function(soFar, product) {
                return soFar.then(function() {
                    return ProductHelper.download(product);
                });
            }, $q.when()).then(function() {
                ProductHelper.writeManifest().finally(function() {
                    $state.go('UserInfo');
                });
            });
        };

        $scope.bypass = function() {
            $state.go('UserInfo');
        }
    });
