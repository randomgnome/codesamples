angular.module('holophotonic')
    .controller('InSessionCtrl', function ($scope, $timeout, $interval, $q, $ionicPlatform, ScanHelper, MediaService) {
        var audioTrack = null, videoInterval = null, stepTimeout = Math.floor($scope.duration / $scope.videos.length);

        console.log($scope.videos);

        $scope.step = -1;
        $scope.audioOn = $scope.enableSound;

        MediaService.fetchAudio().then(function (media) {
            audioTrack = media;
            init();
        });

        $ionicPlatform.ready(function () {
            if (window.screen.lockOrientation) window.screen.lockOrientation('landscape-primary');
        });

        var audioFadeInProgress = false;
        $scope.toggleSound = function() {
            var promise = null;
            if (!audioFadeInProgress) {
                audioFadeInProgress = true;
                if ($scope.audioOn) {
                    promise = MediaService.fadeAudio('out', audioTrack).then(function() {
                        audioTrack.pause();
                    });
                } else {
                    audioTrack.play();
                    promise = MediaService.fadeAudio('in', audioTrack);
                }

                promise.finally(function() {
                    $scope.audioOn = !$scope.audioOn;
                    audioFadeInProgress = false;
                });
            }
        };

        $scope.endSession = function () {
            console.log('Video Session Ending');

            $scope.step = -1;
            MediaService.fadeAudio('out', audioTrack).then(function () {
                console.log('Audio fadeout complete, closing modal');

                $ionicPlatform.ready(function () {
                    if (window.screen.lockOrientation) window.screen.lockOrientation('portrait-primary');
                });

                audioTrack.pause();
                audioTrack.release();

                if (videoInterval != null) {
                    $interval.cancel(videoInterval);
                }

                $scope.close();
            });
        };

        function start() {
            $scope.step = 0;
            videoInterval = $interval(function () {
                $scope.step++;
                console.log('ADVANCING VIDEO');

                if ($scope.step >= $scope.videos.length) {
                    $scope.endSession();
                }
            }, stepTimeout);
        }

        function init() {
            if ($scope.enableSound) audioTrack.play();
            if (!isNaN($scope.duration) && $scope.duration > 0) {
                if ($scope.videos.length > 1) {
                    console.log($scope.videos.length + ' Videos found. Duration is ' + (stepTimeout / 1000) + ' seconds per video. Total session time ' + $scope.duration / 1000 + ' seconds');

                    if ($scope.enableSound) {
                        MediaService.fadeAudio('in', audioTrack).then(function () {
                            console.log('Audio Fadein complete, exposing first video');
                            start();
                        });
                    } else start();

                } else {
                    console.log('Single Video found with specific duration. Total session time ' + ($scope.duration / 1000) + ' seconds');
                    $scope.step = 0;
                    $timeout(function () {
                        $scope.endSession();
                    }, $scope.duration);
                }

            } else { // <-- NO DURATION (INFINITE)

                // Multiple videos. Setup
                if ($scope.videos.length > 1) {
                    var duration = 30;
                    console.log($scope.videos.length + ' videos no duration. ' + duration + ' seconds per video until closed');
                    videoInterval = $interval(function () {

                        console.log('Advancing Video...');
                        $scope.step++;

                        if ($scope.step >= $scope.videos.length) {
                            console.log('OVERWRAP');
                            $scope.step = 0;
                        }
                    }, duration * 1000); // Change vid every 30 seconds
                } else {
                    console.log('Single video. NO Duration. Enjoy.');
                }

                MediaService.fadeAudio('in', audioTrack).then(function () {
                    console.log('Audio fade in complete. Exposing Video');
                    $scope.step = 0;
                });
            }
        }

    });
