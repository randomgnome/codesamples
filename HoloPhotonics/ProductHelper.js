angular.module('holophotonic')
    .service('ProductHelper', function ($q, $cordovaFile, $cordovaFileTransfer, $cordovaZip, $timeout, ModalService, UserCache, _) {
        var directory = 'cdvfile://localhost/persistent/',
            account = UserCache.get('account'),
            manifestName = 'productManifest.json',
            _this = this;

        function createManifest(replace) {
            var defer = $q.defer();
            $cordovaFile.createFile(directory, manifestName, !!replace).then(function(fileData) {
                defer.resolve(fileData);
            }, function(error) {
                defer.reject(error);
            });
            return defer.promise;
        }

        function getManifest() {
            var defer = $q.defer();
            $cordovaFile.readAsText(directory, manifestName).then(function(data) {
                defer.resolve(data.length ? JSON.parse(data) : []);
            }, function(error) {
                defer.reject(error);
            });
            return defer.promise;
        }

        function checkIfManifestExists() {
            var defer = $q.defer();
            $cordovaFile.checkFile(directory, manifestName).then(function () {
                defer.resolve();
            }, function (error) {
                defer.reject(error);
            });
            return defer.promise;
        }

        this.download = function(product, notify) {
            var defer = $q.defer(),
                downloadTarget = 'cdvfile://localhost/temporary/' + product.sku + '.zip';

            $cordovaFileTransfer.download(product.package, downloadTarget, {}, true).then(function() {
                $cordovaZip.unzip(downloadTarget, directory).then(function() {
                    defer.resolve();
                }, function(error) {
                    defer.reject(error);
                });
            }, function(error) {
                defer.reject(error);
            }, function(progress) {
                if (notify) {
                    $timeout(function() {
                       notify((progress.loaded / progress.total) * 100);
                    });
                }
            });
            return defer.promise;
        };

        this.checkForNewProduct = function() {
            var defer = $q.defer();

            _this.refreshAccount();

            checkIfManifestExists().then(function() {
                getManifest().then(function(data) {
                    if (!account || !account.products || !account.products.length) {
                        defer.resolve(null); // No products from server
                    } else {
                        if (!data.length) { // Products From Server, no local products
                            defer.resolve(account.products);
                        } else {
                            var difference = _.differenceBy(account.products, data, 'sku');
                            if (difference && difference.length) {
                                defer.resolve(difference);
                            } else {
                                defer.resolve(null);
                            }
                        }
                    }
                }, function() {
                    defer.reject('Could not read manifest file');
                });
            }, function() {
                createManifest().then(function() {
                    if (account && account.products && account.products.length) defer.resolve(account.products);
                    else defer.resolve(null);
                }, function() {
                    defer.reject('Could not create manifest');
                });
            });
            return defer.promise;
        };

        this.refreshAccount = function() {
            account = UserCache.get('account');
        };

        this.writeManifest = function() {
            var defer = $q.defer();

            if (!account || !account.products || !account.products.length) defer.reject('No Data To Write');

            $cordovaFile.writeFile(directory, manifestName, JSON.stringify(account.products), true).then(function(data) {
                defer.resolve(data);
            }, function(error) {
                defer.reject(error);
            });
            return defer.promise;
        };

        this.hasProducts = function() {
            return (account && account.products && account.products.length);
        };

        this.getLocalProducts = function() {
            var defer = $q.defer();
            getManifest().then(function(data) {
                defer.resolve(data.reduce(function(soFar, product) {

                    product.image = directory + product.image;
                    product.video = directory + product.video;
                    product.label = product.name;

                    soFar.push(product);
                    return soFar;
                }, []));
            }, function() {
                defer.resolve(null);
            });
            return defer.promise;
        };

        this.showProducts = function() {
            _this.getLocalProducts().then(function(products) {
                ModalService.show('js/components/Product/product.html', {
                    products: products
                });
            }).monitor();
        };

    });
