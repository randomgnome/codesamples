<?php

namespace App\Models;

use App\Models\Device;
use App\Models\Product;
use App\Models\SystemSetting;
use Illuminate\Validation\ValidationException;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Account extends Eloquent {

    /*
      first:       (string)         First Name
      last:        (string)         Last Name
      email:       (string)         Email Address the account belongs to
      active:      (bool)           Flag for account status
      activation:  (date)           Date that the account was activated
      devices:     (array<Devices>) List of associated devices
      infusionId:  (int)            Infusionsoft Contact ID
     */

    protected $dates = ['created', 'activation'];

    public function __construct() {
        parent::__construct();
        $this->active = false;
        $this->maxDevices = (int)env('MAX_DEVICES', 3);
        $this->trialPeriod = (int)env('TRIAL_PERIOD', 7);
        $this->devices = [];
    }

    public function devices() {
        return $this->embedsMany('App\Models\Device');
    }

    public function products() {
        return $this->embedsMany('App\Models\Product');
    }

    public function hasUUID($uuid) {
        $devices = $this->devices()->get();
        if (count($devices) == 0) return false;
        foreach ($devices as $device) {
            if ($device->uuid && $device->uuid == $uuid)
                return true;
        }
        return false;
    }

    public function addDevice($uuid, $saveNow = false) {
        //if (count($this->devices()) >= $this->maxDevices) throw new ValidationException("Max Devices Reached. No new devices can be added");
        if ($this->hasUUID($uuid)) throw new ValidationException("UUID is already associated to device");

        $device = new Device();
        $device->uuid = $uuid;
     
        if ($saveNow) $this->devices()->save($device);
        else $this->devices()->associate($device);
    }

    public function hasSKU($sku) {
        $products = $this->products()->get();
        if (count($products) == 0) return false;
        foreach ($products as $product) {
            if ($product->sku && $product->sku == $sku)
                return true;
        }
        return false;
    }
    public function addProduct($sku, $saveNow = false) {
        $product = Product::findBySku($sku);
        if (!$product) throw new ValidationException('Invalid Product');
        if ($this->hasSKU($sku)) throw new ValidationException('Account already has that product');

        $product = Product::copy($product);
        if ($saveNow) $this->products()->save($product);
        else $this->products()->associate($product);
    }

    public function infusionSync($InfusionContactID=null) {
        if (SystemSetting::has('inusionToken')) {
            $infusion = infusion();

            $InfusionContactID = $this->infusionId == null ? $InfusionContactID : $this->infusionId;

            if ($InfusionContactID) {
                $infusion->contacts()->update($InfusionContactID, ['_mongoId' => $this->id]);
            } else {
                $InfusionContactID = $infusion->contacts()->addWithDupCheck([
                    'FirstName' => $this->first,
                    'LastName' => $this->last,
                    'Email' => $this->email,
                    '_mongoId' => $this->id
                ], 'Email');
            }

            // If we have an Infusion Contact ID
            if (!$this->infusionId && $InfusionContactID && is_numeric($InfusionContactID)) {

                // Add Infusion ID to Account
                $this->infusionId = $InfusionContactID;
                
                // Add Interest Tag to Contact
                $infusion->contacts()->addToGroup($InfusionContactID, 465);
                
                // Save Account
                $this->save();
            }
        }
    }
}

?>