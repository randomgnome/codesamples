angular.module('holophotonic')
    .service('AccountHelper', function ($q, $state, $ionicLoading, $ionicPopup, $cordovaAppVersion, $cordovaInAppBrowser, UserCache, ProductHelper, AccountService, AppService, moment, _) {
        function isAccountValid(account) {
            return account.active == true || moment().diff(account.created_at, 'days') <= account.trialPeriod;
        }

        function checkForAppUpdate() {
            var defer = $q.defer();
            $cordovaAppVersion.getVersionNumber().then(function (localVersion) {
                AppService.getVersion().then(function(remoteVersion) {
                    var local = localVersion.split('.'),
                        remote = remoteVersion.split('.'),
                        ret = false;

                    // Compare each part of the version number; Eg: 1.0.0 vs 1.1.0
                    for (var i = 0; i < remote.length; i++) {
                        if (!ret) {
                            if (!local[i]) ret = true;
                            else if (remote[i] > local[i]) ret = true;
                        }
                    }

                    defer.resolve(ret);
                }, function() {
                    defer.resolve(false);
                });
            }, function() {
                defer.resolve(false);
            });
            return defer.promise;
        }

        function processLoadAction(account, addDevice) {
            checkForAppUpdate().then(function(isUpdateAvailable) {
                if (isUpdateAvailable) {
                    $ionicPopup.confirm({
                        title: 'Update',
                        template: 'An update is available. Would you like to update now?'
                    }).then(function (res) {
                        if (res) {
                            $cordovaInAppBrowser.open('http://download.fractalhologram.com', '_system');
                        }
                    });
                }
            });

            if (account) {

                $ionicLoading.show({
                    hideOnStateChange: true,
                    delay: 0
                });

                UserCache.put('account', account);
                if (!!addDevice) AccountService.addDevice();

                if (isAccountValid(account)) {
                    ProductHelper.checkForNewProduct().then(function(products) {
                        if (products) $state.go('ProductDownload');
                        else $state.go('UserInfo');
                    }, function() {
                        $state.go('UserInfo');
                    });
                } else {
                    $state.go('Purchase');
                }
            } else {
                $state.go('Login');
            }

            $ionicLoading.hide();
        }

        var flattenErrors = function (err) {
            return _.isObject(err) ? _.flatten(_.map(err, _.pluck)).join(',') : err;
        };

        this.loadAction = function () {
            AccountService.searchByUUID().then(function (account) {
                processLoadAction(account);
            }, function () {
                processLoadAction(UserCache.get('account'));
            });
        };

        this.login = function (email) {
            var defer = $q.defer();
            AccountService.searchByEmail(email).then(function (account) {
                processLoadAction(account, true);
                defer.resolve(account);
            }, function (err) {
                defer.reject(flattenErrors(err));
            });
            return defer.promise;
        };

        this.register = function (params) {
            var defer = $q.defer();

            try {

                if (!params.first || !params.last || !params.email) throw 'First, Last and Email are required';

                AccountService.register(params).then(function (account) {
                    processLoadAction(account, true);
                    defer.resolve(account);
                }, function (err) {
                    defer.reject(flattenErrors(err));
                });

            } catch (e) {
                defer.reject(e);
            }

            return defer.promise;
        };
    });
