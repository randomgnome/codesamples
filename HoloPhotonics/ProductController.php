<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class ProductController extends Controller
{

    public function returnOK() {
        return response('OK', 200)->json();
    }

    public function fetch($id) {
        $product = Product::find($id);

        // Validate
        if ($product == null) throw new ValidationException('Invalid Product Id');

        // Return
        return response()->json(['success' => true, 'data' => ['product' => $product]]);
    }

    public function search(Request $request) {
        // Validate Data
        $this->validate($request, ['term' => 'required'], ['required' => ':attribute is required']);

        // Fetch Data from Request
        $term = $request->input('term');

        // Fetch and return
        $product = Product::where('name', 'regexp', '/' . $term . '/i')->orWhere('sku', 'regexp', '/' . $term . '/i')->get();

        if (!$product) throw new ValidationException('No Results');

        return response()->json(['success' => true, 'data' => ['product' => $product]]);
    }
    
    public function fetchBySku(Request $request) {
        // Validate Data
        $this->validate($request, ['sku' => 'required'], ['required' => ':attribute is required']);

        // Fetch Data from Request
        $sku = $request->input('sku');

        // Fetch and return product
        $product = Product::where('sku', '=', strtoupper($sku))->first();
        if (!$product) throw new ValidationException('No Results');
        return response()->json(['success' => true, 'data' => ['product' => $product]]);
    }

    public function paginate(Request $request) {
        // Fetch Data From Request
        $limit = (int)$request->input('limit', 15);
        $page = (int)$request->input('page', 1);

        // Get Total Accounts
        $total = Product::count();

        // Get Account Page
        $accounts = Product::skip(($page - 1) * $limit)->take($limit)->get();

        // Return
        return response()->json(['success' => true, 'data' => ['products' => $accounts, 'total' => $total, 'page' => $page, 'limit' => $limit]]);
    }


    public function update($id, Request $request) {
        $this->validate($request, [
            'image' => 'mimes:png',
            'video' => 'mimes:mp4', 
            'sku' => 'regex:/[a-zA-Z][a-zA-Z0-9]+/',
            'price' => 'numeric'
        ], [
            'mimes' => ':attribute is an invlid file type',
            'numeric' => ':attribute must be numeric', 
            'regex' => ':attribute must start with a letter and only contain letters and numbers'
        ]);


        $product = Product::find($id);
        if (!$product) throw new ValidationException('Invalid Product Id');

        // Fetch Data from Request
        $image = $request->input('image');
        $video = $request->input('video');
        $name  = $request->input('name');
        $words = $request->input('words');
        $price = $request->input('price');
        $longDesc = $request->input('longDesc');
        $shortDesc = $request->input('shortDesc');
        $addToNew = filter_var($request->input('autoAdd', false), FILTER_VALIDATE_BOOLEAN);
        
        // Update Product
        if ($image) $product->image = $image;
        if ($video) $product->video = $video;
        if ($name) $product->name = $name;
        if ($words) $product->words = $words;
        if ($price) $product->price = $price;
        if ($longDesc) $longDesc = $request->input('longDesc');
        if ($shortDesc) $shortDesc = $request->input('shortDesc');
        $product->autoAdd = $addToNew;

        if ($image || $video) {
            // Process Files (Image & Video)
            $product->processFiles($request);
            
            // Generate Zip File
            $product->generateArchive();
        }
        
        // Update Infusion Product if we have the Id
        $product->infusionSync();

        // Update Local Product
        $product->save();

        // Response
        return response()->json(['success' => true, 'data' => ['product' => $product]]);

    }

    public function create(Request $request) {
        $this->validate($request, [
            'image' => 'required|mimes:png',
            'video' => 'required|mimes:mp4', 
            'name' => 'required', 
            'sku' => 'required|regex:/[a-zA-Z][a-zA-Z0-9]+/',
            'price' => 'required|numeric'
        ], [
            'required' => ':attribute is required',
            'mimes' => ':attribute is an invlid file type',
            'numeric' => ':attribute must be numeric', 
            'regex' => ':attribute must start with a letter and only contain letters and numbers'
        ]);

        // Fetch Data from Request
        $name  = $request->input('name');
        $words = $request->input('words');
        $price = $request->input('price');
        $sku   = strtoupper($request->input('sku'));
        $longDesc = $request->input('longDesc');
        $shortDesc = $request->input('shortDesc');

        $addToExisting = filter_var($request->input('addToActive', false), FILTER_VALIDATE_BOOLEAN);
        $addToNew = filter_var($request->input('autoAdd', false), FILTER_VALIDATE_BOOLEAN);

        // Check if SKU exists
        if (Product::findBySku($sku) != null) throw new ValidationException('SKU is already taken');

        // Create new product
        $product = new Product();
        $product->image = strtolower($sku) . '.png';
        $product->video = strtolower($sku) . '.mp4';
        $product->name = $name;
        $product->words = $words;
        $product->sku = $sku;
        $product->price = (float)$price;
        $product->longDesc = $longDesc;
        $product->shortDesc = $shortDesc;
        $product->autoAdd = $addToNew;

        // Process Files (Image & Video)
        $product->processFiles($request, true);

        // Generate Zip File
        $product->generateArchive();

        // Save Product
        $product->save();
        
        // Sync to Infusion
        $product->infusionSync();

        // Add new product to all existing active accounts
        if ($addToExisting) {
            foreach (Account::where('active', '=', true)->get() as $account) {
                try {
                    $account->addProduct($sku, true);
                } catch (Exception $e) {
                    // Do Nothing
                }
            }
        }

        // Response
        return response()->json(['success' => true, 'data' => ['product' => $product]]);
    }
}