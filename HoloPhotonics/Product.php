<?php

namespace App\Models;

use ZipArchive;
use App\Models\SystemSetting;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class Product extends Eloquent {
    /*
      package: (string) Remote location for .zip file containing image/video assets
      image:   (string) Image path within package
      video:   (string) Video path within package
      name:    (string) Product's name
      sku:     (string) Unique product SKU
      words:   (string) Product "Words"

      infusionId: (int) Infusionsoft Id for product
      price:      (float) Product Price
      shortDesc: (string) Infusion Short Desc (may not be in sync)
      longDesc: (string) Infusion Long Desc (may not be in sync)
      autoAdd: (bool) Flag to determine if we should automatically add to new accounts
     */

    const INFUSION_CATEGORY = 20;

    public static function findBySku($sku) {
        return self::where('sku', '=', strtoupper($sku))->first();
    }

    public static function findByInfusionId($id) {
        return self::where('infusionId', '=', (int)$id)->first();
    }

    public static function copy(Product $source) {
        $product = new Product();

        $product->image = $source->image;
        $product->video = $source->video;
        $product->name = $source->name;
        $product->words = $source->words;
        $product->sku = $source->sku;
        $product->price = $source->price;
        $product->longDesc = $source->longDesc;
        $product->shortDesc = $source->shortDesc;
        $product->package = $source->package;
        
        return $product;
    }

    // Generates a .zip file with video and image files in it
    public function generateArchive() {
        // Determine Zip Path
        $zipPath = app_public() . '/z/' . strtolower($this->sku) . '.zip';
        
        // Open Zip File, Process error if opening fails
        $zip = new ZipArchive();
        if (($result=$zip->open($zipPath, ZipArchive::OVERWRITE | ZipArchive::CREATE)) !== TRUE) {
            $reason = 'Unknown Failure';

            switch ($result) {
            case ZipArchive::ER_EXISTS: $reason = 'File already exists'; break;
            case ZipArchive::ER_INCONS: $reason = 'File Inconsistent'; break;
            case ZipArchive::ER_INVAL: $reason = 'Invalid Argument'; break;
            case ZipArchive::ER_MEMORY: $reason = 'Malloc Failure'; break;
            case ZipArchive::ER_NOENT: $reason = 'No Such File'; break;
            case ZipArchive::ER_NOZIP: $reason = 'Not A Zip Archive'; break;
            case ZipArchive::ER_OPEN: $reason = 'Can\'t Open File'; break;
            case ZipArchive::ER_READ: $reason = 'Read Error'; break;
            case ZipArchive::ER_SEEK: $reason = 'Seek Error'; break;
            }

            throw new ValidationException('Unable to create product package: ' . $reason);
        }

        // Add Files to Zip
        $zip->addFile($this->getFullImagePath(), $this->getImageFileName());
        $zip->addFile($this->getFullVideoPath(), $this->getVideoFileName());

        // Close Zip
        $zip->close();

        // Set Zip public URL on Product
        $this->package = str_replace(app_public() . '/', env('ACCESS_POINT'), $zipPath);
    }

    // Process Image & Video from request
    public function processFiles(Request $request, $strict=false) {
        // Fetch Files from $request
        $image = $request->file('image', null);
        $video = $request->file('video', null);

        // Validate File Uploads and throw exception if in strict mode
        if ($strict && (!$image->isValid() || !$video->isValid())) {
            throw new ValidationException('File Upload Failed');
        }

        // Move files to perm location if files are valid
        if ($image && $image->isValid()) $image->move($this->getImagePath(), $this->getImageFileName());
        if ($video && $video->isValid()) $video->move($this->getVideoPath(), $this->getVideoFileName());
    }
    
    // Syncs product to Infusionsoft
    public function infusionSync() {
        if (SystemSetting::has('inusionToken')) {
            $infusion = infusion();
            if ($this->infusionId) {
                $update = [
                    'ProductPrice' => $this->price,
                    'ProductName' => $this->name
                ];

                if ($this->longDesc) $update['Description'] = $this->longDesc;
                if ($this->shortDesc) $update['ShortDescription'] = $this->shortDesc;

                $infusion->data()->update('Product', $this->infusionId, $update);
            } else {

                // Create Infusion Product
                $data = [
                    'ProductPrice' => $this->price,
                    'ProductName' => $this->name,
                    'Sku' => $this->sku,
                    'CityTaxable' => 0,
                    'CountryTaxable' => 0,
                    'HideInStore' => 0,
                    'NeedsDigitalDelivery' => 0,
                    'Shippable' => 0,
                    'StateTaxable' => 0,
                    'Status' => 1,
                    'Taxable' => 0
                ];

                if ($this->longDesc) $data['Description'] = $this->longDesc;
                if ($this->shortDesc) $data['ShortDescription'] = $this->shortDesc;

                $this->infusionId = $infusion->data()->add('Product', $data);

                if ($this->infusionId) {
                    // Associate Infusion Product to Category
                    $infusion->data()->add('ProductCategoryAssign', ['ProductId' => $this->infusionId, 'ProductCategoryId' => self::INFUSION_CATEGORY]);
                }
            }
        
            $this->save();
        }
    }

    // Internal Methods
    private function getImagePath() {
        return app_public() . '/i';
    }

    private function getImageFileName() {
        return strtolower($this->sku) . '.png';
    }

    private function getFullImagePath() {
        return $this->getImagePath() . '/' . $this->getImageFileName();
    }

    private function getVideoPath() {
        return app_public() . '/v';
    }

    private function getVideoFileName() {
        return strtolower($this->sku) . '.mp4';
    }

    private function getFullVideoPath() {
        return $this->getVideoPath() . '/' . $this->getVideoFileName();
    }
}

?>