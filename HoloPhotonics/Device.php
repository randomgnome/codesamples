<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Device extends Eloquent {
    /*
      uuid:       (string)   Device Unique Identifier
      created_at: (DateTime) DateTime that the device was created
      updated_at: (DateTime) DateTime that the device was last updated
     */
}

?>