<?php namespace modules\analytic;
class Model {

	public function fetchAnalyticObject(array $params) {
		try {
			switch (consume('e', $params)) {
			case 1:
				$e = \DBO\Pin::fetch(consume('i', $params, 0));
				$e = $e->fetchAnalytic();
				break;
				
			case 2:
				$e = \DBO\Ad::fetch(consume('i', $params, 0));
				$e = $e->fetchAnalytic();
				break;
				
			case 3:
				$e = \DBO\Account\Category::fetch(consume('i', $params, 0));
				$e = $e->fetchAnalytic();
				break;
			}
			
			return $e;
		} catch (\Exception $e) {
			return false;
		}
	}

}
