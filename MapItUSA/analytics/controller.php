<?php namespace modules\analytic;

class Controller extends \Controller {
	protected $default_data = array();
	protected $name = 'analytic';

	public function __construct(&$params) {
		$this->processContext(consume('context',$params), $params);
	}

	protected function context(\DeguArrayIterator $iter, &$params = NULL) {
		$json = array('success' => true);
		try {
			switch (strtolower($iter->current())) {
			case 'track':
				$a = consume('a', $params, array());
				$redir  = consume('r', $params, false); // Redirect after tracking?

				if (count($a)) {
					foreach ($a as $track) {
						try {
							$entity = consume('e', $track); // Which Entity are we tracking
							$id     = consume('i', $track); // The Entity's ID
							$type   = consume('t', $track); // What type of event are we tracking?
							
							$this->track($entity, $id, $type);					
						} catch (\Exception $e) {}
					}
				} else {
					
					$entity = consume('e', $params); // Which Entity are we tracking
					$id     = consume('i', $params); // The Entity's ID
					$type   = consume('t', $params); // What type of event are we tracking?
					
					if (!$entity) throw new \Exception('Missing Entity Type (e)');
					if (!$id)     throw new \Exception('Missing Entity ID (i)');
					if (!$type)   throw new \Exception('Missing Event Type (t)');
					
					$this->track($entity, $id, $type);					
				}

				if ($redir) {
					header('Location: ' . $redir);
					die();
				}

				break;
			}
		} catch (\Exception $e) {
			$json = array('success' => false, 'error' => $e->getMessage());
		}

		die(json_encode($json));
	}

	private function track($entity, $id, $type) {
		try {
			$e = false;
			switch ($entity) {
			case 1:
				$e = \DBO\Pin::fetch($id);
				$e = $e->fetchAnalytic();
				break;

			case 2:
				$e = \DBO\Ad::fetch($id);
				$e = $e->fetchAnalytic();
				break;

			case 3:
				$e = \DBO\Account\Category::fetch($id);
				$e = $e->fetchAnalytic();
				break;
				
			default: throw new \Exception('Invalid Entity');
			}

			switch ($type) {
			case 1:
				$e->trackEvent(\DBO\Analytics\Event\Type::IMPRESSION());
				break;

			case 2:
				$e->trackEvent(\DBO\Analytics\Event\Type::CLICK());				
				break;

			case 3:
				$e->trackEvent(\DBO\Analytics\Event\Type::CLICKTHRU());
				break;

			default: throw new \Exception('Invalid Type');
			}
			
		} catch (\Exception $e) { throw $e; }
	}
}