<?php namespace DBO\Analytics\Event;

class Type extends \DBO3 {
	const TABLE = 'AnalyticsType';
	
	static public function _InitLocalColumns() {
		if (__CLASS__==get_called_class()) {
			static::addColumn(__CLASS__,
							  \DBO\Column::init('name')
							  ->attributes(\DBO\Column::VARCHAR|\DBO\Column::NO_NULL)
							  ->preProcess(\DBO\Callback::trim()->nullIfEmptyString())
							  ->size(255)
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('description')
							  ->attributes(\DBO\Column::VARCHAR|\DBO\Column::NULL_OK)
							  ->preProcess(\DBO\Callback::trim()->nullIfEmptyString())
							  ->size(255)
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('status')
							  ->attributes(\DBO\Column::ENUM|\DBO\Column::NO_NULL)
							  ->datatype(array('Enabled','Disabled'))
			);
		}
	}

	static public function IMPRESSION() { return static::fetch(1); }
	static public function CLICK() { return static::fetch(2); }
	static public function CLICKTHRU() { return static::fetch(3); }
}
