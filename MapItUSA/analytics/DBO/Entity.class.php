<?php namespace DBO\Analytics;

class Entity extends \DBO3 {
	const LOOKUP_KEY = 'id';
	// Expected constants - With examples
	// const _A_Node_Class = '\DBO\User\Ad';
	// const _A_Node_Member = 'UserAd';

	static public function _sanity_dependencies() {
		$class = static::_A_Node_Class; $class::_sanity();
	}

	static public function _InitDynamicColumns($class) {
		$class::addColumn($class,
						  \DBO\Column::init($class::_A_Node_Member,$class::_A_Node_Member.'ID')
						  ->attributes(\DBO\Column::BIGINT|\DBO\Column::UNSIGNED|\DBO\Column::NO_NULL)
						  ->size(20)
						  ->relation(\DBO\Relation::init()
									 ->localKey($class::_A_Node_Member)
									 ->localMember($class::_A_Node_Member)
									 ->localClass($class)
									 ->foreignClass($class::_A_Node_Class)
									 ->foreignKey('id')
						  )
		);
	}
	static public function _InitLocalColumns() {
		if (__CLASS__==get_called_class()) {
			static::addColumn(__CLASS__,
							  \DBO\Column::init('impressions')
							  ->attributes(\DBO\Column::BIGINT|\DBO\Column::UNSIGNED|\DBO\Column::NO_NULL)
							  ->preProcess(\DBO\Callback::trim()->nullIfEmptyString())
							  ->default(0)
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('clicks')
							  ->attributes(\DBO\Column::BIGINT|\DBO\Column::UNSIGNED|\DBO\Column::NO_NULL)
							  ->preProcess(\DBO\Callback::trim()->nullIfEmptyString())
							  ->default(0)
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('clickthru')
							  ->attributes(\DBO\Column::BIGINT|\DBO\Column::UNSIGNED|\DBO\Column::NO_NULL)
							  ->preProcess(\DBO\Callback::trim()->nullIfEmptyString())
							  ->default(0)
			);
		}
	}

}
