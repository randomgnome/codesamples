<?php namespace DBO\Analytics;

abstract class Event extends \DBO3 {
	const LOOKUP_KEY = 'id';
	// Expected constants - with examples
	// const _AE_Node_Class = '\DBO\User\Ad';
	// const _AE_Node_Member = 'UserAd';
	
	static public function _sanity_dependencies() {
		\DBO\Analytics\Event\Type::_sanity();
		$class = static::_AE_Node_Class; $class::_sanity();
	}

	static public function _InitDynamicColumns($class) {
	$class::addColumn($class,
						  \DBO\Column::init($class::_AE_Node_Member,$class::_AE_Node_Member.'ID')
						  ->attributes(\DBO\Column::BIGINT|\DBO\Column::UNSIGNED|\DBO\Column::NO_NULL)
						  ->size(20)
						  ->relation(\DBO\Relation::init()
									 ->localKey($class::_AE_Node_Member)
									 ->localMember($class::_AE_Node_Member)
									 ->localClass($class)
									 ->foreignClass($class::_AE_Node_Class)
									 ->foreignKey('id')
						  )
		);
	}
	
	static public function _InitLocalColumns() {
		if (__CLASS__==get_called_class()) {
			static::addColumn(__CLASS__,
							  \DBO\Column::init('Type','TypeID')
							  ->attributes(\DBO\Column::BIGINT|\DBO\Column::UNSIGNED|\DBO\Column::NO_NULL)
							  ->size(20)
							  ->relation(\DBO\Relation::init()
										 ->localKey('Type')
										 ->localMember('Type')
										 ->localClass(get_called_class())
										 ->foreignClass('\DBO\Analytics\Event\Type')
										 ->foreignKey('id')
							  )
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('when')
							  ->attributes(\DBO\Column::DATETIME|\DBO\Column::NO_NULL)
							  ->preProcess(\DBO\Callback::trim()->nullIfEmptyString())
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('ip')
							  ->attributes(\DBO\Column::BIGINT|\DBO\Column::UNSIGNED|\DBO\Column::NO_NULL)
							  ->preProcess(\DBO\Callback::trim()->nullIfEmptyString())
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('details')
							  ->attributes(\DBO\Column::BLOB|\DBO\Column::NULL_OK)
							  ->preProcess(\DBO\Callback::trim()->nullIfEmptyString())
			);
		}
	}
}
