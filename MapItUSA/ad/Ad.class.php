<?php namespace DBO;

class Ad extends \DBO3 {
	const LOOKUP_KEY = 'id';
	const TABLE = 'Ad';
	
	static public function _sanity_dependencies() {
		\DBO\File::_sanity();
	}

	static public function _InitLocalColumns() {
		if (__CLASS__==get_called_class()) {
			static::addColumn(__CLASS__,
							  \DBO\Column::init('name')
							  ->attributes(\DBO\Column::VARCHAR|\DBO\Column::NO_NULL)
							  ->preProcess(\DBO\Callback::trim()->nullIfEmptyString())
							  ->size(255)
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('start')
							  ->attributes(\DBO\Column::DATETIME|\DBO\Column::NULL_OK)
							  ->preProcess(\DBO\Callback::trim()->nullIfEmptyString())
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('end')
							  ->attributes(\DBO\Column::DATETIME|\DBO\Column::NULL_OK)
							  ->preProcess(\DBO\Callback::trim()->nullIfEmptyString())
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('status')
							  ->attributes(\DBO\Column::ENUM|\DBO\Column::NO_NULL)
							  ->datatype(array('Enabled','Disabled','Suspended'))
							  ->default('Enabled')
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('exposureType')
							  ->attributes(\DBO\Column::ENUM|\DBO\Column::NO_NULL)
							  ->datatype(array('Front','Back','Both'))
							  ->default('Both')
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('description')
							  ->attributes(\DBO\Column::VARCHAR|\DBO\Column::NULL_OK)
							  ->preProcess(\DBO\Callback::trim()->nullIfEmptyString())
							  ->size(255)
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('url')
							  ->attributes(\DBO\Column::TEXT|\DBO\Column::NO_NULL)
							  ->preProcess(\DBO\Callback::trim()->nullIfEmptyString())
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('params')
							  ->attributes(\DBO\Column::TEXT|\DBO\Column::NULL_OK)
							  ->preProcess(\DBO\Callback::trim()->nullIfEmptyString())
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('Image','ImageID')
							  ->attributes(\DBO\Column::BIGINT|\DBO\Column::UNSIGNED|\DBO\Column::NO_NULL)
							  ->size(20)
							  ->relation(\DBO\Relation::init()
										 ->localKey('Image')
										 ->localMember('Image')
										 ->localClass(__CLASS__)
										 ->foreignClass('\DBO\File')
										 ->foreignKey('id')
							  )
			);
		}
	}

	// Analytics
	public function fetchAnalytic() {
		$search = \DBI\Search::init(\DBO\Ad\Analytics::TABLE)
			->where('Ad', \DBI\Condition::EQ, $this->id)
			->limit(1);

		$out = NULL;
		try { 
			$out = \DBO\Ad\Analytics::fetchOne($search); 
			if (!$out instanceof \DBO\Ad\Analytics) throw new \DBI\Exception();
		} catch (\DBI\Exception $e) { $out = \DBO\Ad\Analytics::createObj(array('Ad' => $this->id)); }

		return $out;
	}

}
