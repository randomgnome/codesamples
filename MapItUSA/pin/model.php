<?php namespace modules\point;
class Model {

	// Fetches enabled categories for a specific type
	public function fetchEnabledCategoriesForType($type, $account=NULL) {
		$account = $account instanceof \DBO\Account ? $account : \DBO\Account::SessionAccount();
		if (is_numeric($type)) return $account->EnabledCategoriesByType($type);
		else return array();
	}
	
}
