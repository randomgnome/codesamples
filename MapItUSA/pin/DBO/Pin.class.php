<?php namespace DBO;

class Pin extends \DBO3 {
	const LOOKUP_KEY = 'id';
	const TABLE = 'Pin';
	
	static public function _sanity_dependencies() {
		\DBO\Address::_sanity();
		\DBO\Account::_sanity();
		\DBO\Pin\Type::_sanity();
		\DBO\File::_sanity();
	}
	
	static public function _InitLocalColumns() {
		if (__CLASS__==get_called_class()) {
			static::addColumn(__CLASS__,
							  \DBO\Column::init('Account','AccountID')
							  ->attributes(\DBO\Column::BIGINT|\DBO\Column::UNSIGNED|\DBO\Column::NO_NULL)
							  ->size(20)
							  ->relation(\DBO\Relation::init()
										 ->localKey('Account')
										 ->localMember('Account')
										 ->localClass(__CLASS__)
										 ->foreignClass('\DBO\Account')
										 ->foreignKey('id')
							  )
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('Owner','OwnerID')
							  ->attributes(\DBO\Column::BIGINT|\DBO\Column::UNSIGNED|\DBO\Column::NULL_OK)
							  ->size(20)
							  ->relation(\DBO\Relation::init()
										 ->localKey('Owner')
										 ->localMember('Owner')
										 ->localClass(__CLASS__)
										 ->foreignClass('\DBO\User')
										 ->foreignKey('id')
							  )
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('Type','TypeID')
							  ->attributes(\DBO\Column::BIGINT|\DBO\Column::UNSIGNED|\DBO\Column::NO_NULL)
							  ->size(20)
							  ->relation(\DBO\Relation::init()
										 ->localKey('Type')
										 ->localMember('Type')
										 ->localClass(__CLASS__)
										 ->foreignClass('\DBO\Pin\Type')
										 ->foreignKey('id')
							  )
			);
			if (false) // Hopefully this won't be necessary
				static::addColumn(__CLASS__,
								  \DBO\Column::init('Level','LevelID')
								  ->attributes(\DBO\Column::BIGINT|\DBO\Column::UNSIGNED|\DBO\Column::NO_NULL)
								  ->size(20)
								  ->relation(\DBO\Relation::init()
											 ->localKey('Level')
											 ->localMember('Level')
											 ->localClass(__CLASS__)
											 ->foreignClass('\DBO\Pin\Level')
											 ->foreignKey('id')
								  )
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('Address','AddressID')
							  ->attributes(\DBO\Column::BIGINT|\DBO\Column::UNSIGNED|\DBO\Column::NULL_OK)
							  ->size(20)
							  ->relation(\DBO\Relation::init()
										 ->localKey('Address')
										 ->localMember('Address')
										 ->localClass(__CLASS__)
										 ->foreignClass('\DBO\Address')
										 ->foreignKey('id')
							  )
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('name')
							  ->attributes(\DBO\Column::VARCHAR|\DBO\Column::NULL_OK)
							  ->preProcess(\DBO\Callback::trim()->nullIfEmptyString())
							  ->size(255)
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('start')
							  ->attributes(\DBO\Column::DATETIME|\DBO\Column::NULL_OK)
							  ->preProcess(\DBO\Callback::trim()->nullIfEmptyString())
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('end')
							  ->attributes(\DBO\Column::DATETIME|\DBO\Column::NULL_OK)
							  ->preProcess(\DBO\Callback::trim()->nullIfEmptyString())
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('status')
							  ->attributes(\DBO\Column::ENUM|\DBO\Column::NO_NULL)
							  ->datatype(array('Enabled','Disabled','Suspended'))
							  ->default('Enabled')
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('zoom_min')
							  ->attributes(\DBO\Column::TINYINT|\DBO\Column::NO_NULL|\DBO\Column::UNSIGNED)
							  ->size(3)
							  ->default(0)
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('zoom_max')
							  ->attributes(\DBO\Column::TINYINT|\DBO\Column::NO_NULL|\DBO\Column::UNSIGNED)
							  ->size(3)
							  ->default(21)
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('zoombits')
							  ->attributes(\DBO\Column::INT|\DBO\Column::NO_NULL)
							  ->size(11)
							  ->default(0)
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('pin_icon')
							  ->attributes(\DBO\Column::VARCHAR|\DBO\Column::NULL_OK)
							  ->preProcess(\DBO\Callback::trim()->nullIfEmptyString())
							  ->size(255)
			);
			// These probobly shouldn't be on the pin table. But whatever.
			static::addColumn(__CLASS__,
							  \DBO\Column::init('home_business')
							  ->attributes(\DBO\Column::TINYINT|\DBO\Column::NO_NULL|\DBO\Column::UNSIGNED)
							  ->size(1)
							  ->default(0)
			);
			static::addColumn(__CLASS__, // Imported column from original mapit. This probobly shouldn't even be a column in our dbo structure. Garbage.
							  \DBO\Column::init('event_date')
							  ->attributes(\DBO\Column::DATETIME|\DBO\Column::NULL_OK)
							  ->preProcess(\DBO\Callback::trim()->nullIfEmptyString())
			);
			static::addColumn(__CLASS__, // Imported column from original mapit. This probobly shouldn't even be a column in our dbo structure. Garbage.
							  \DBO\Column::init('start_time')
							  ->attributes(\DBO\Column::DATETIME|\DBO\Column::NULL_OK)
							  ->preProcess(\DBO\Callback::trim()->nullIfEmptyString())
			);
			static::addColumn(__CLASS__, // Imported column from original mapit. This probobly shouldn't even be a column in our dbo structure. Garbage.
							  \DBO\Column::init('expire_time')
							  ->attributes(\DBO\Column::DATETIME|\DBO\Column::NULL_OK)
							  ->preProcess(\DBO\Callback::trim()->nullIfEmptyString())
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('chamber')
							  ->attributes(\DBO\Column::TINYINT|\DBO\Column::NO_NULL|\DBO\Column::UNSIGNED)
							  ->size(1)
							  ->default(0)
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('shop_local')
							  ->attributes(\DBO\Column::TINYINT|\DBO\Column::NO_NULL|\DBO\Column::UNSIGNED)
							  ->size(1)
							  ->default(0)
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('business_alliance')
							  ->attributes(\DBO\Column::TINYINT|\DBO\Column::NO_NULL|\DBO\Column::UNSIGNED)
							  ->size(1)
							  ->default(0)
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('business_licence')
							  ->attributes(\DBO\Column::TINYINT|\DBO\Column::NO_NULL|\DBO\Column::UNSIGNED)
							  ->size(1)
							  ->default(0)
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('show_on_map')
							  ->attributes(\DBO\Column::TINYINT|\DBO\Column::NO_NULL|\DBO\Column::UNSIGNED)
							  ->size(1)
							  ->default(1)
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('Logo','LogoID')
							  ->attributes(\DBO\Column::BIGINT|\DBO\Column::UNSIGNED|\DBO\Column::NULL_OK)
							  ->size(20)
							  ->relation(\DBO\Relation::init()
										 ->localKey('Logo')
										 ->localMember('Logo')
										 ->localClass(__CLASS__)
										 ->foreignClass('\DBO\File')
										 ->foreignKey('id')
							  )
			);
			static::addColumn(__CLASS__,
							  \DBO\Column::init('DealImage','DealImageID')
							  ->attributes(\DBO\Column::BIGINT|\DBO\Column::UNSIGNED|\DBO\Column::NULL_OK)
							  ->size(20)
							  ->relation(\DBO\Relation::init()
										 ->localKey('DealImage')
										 ->localMember('DealImage')
										 ->localClass(__CLASS__)
										 ->foreignClass('\DBO\File')
										 ->foreignKey('id')
							  )
			);
		}
	}
	
	static public function _InitLocalRelations() {
		if (__CLASS__ == get_called_class()) {
			$class = get_called_class();
			static::AddRelation($class,
								\DBO\Relation::init()
								->localKey('Fields')
								->reversedMember('Fields')
								->postLinkKey('id')
								->foreignKey('Pin')
								->foreignClass('\DBO\Pin\Field')
			);
			static::AddRelation($class,
								\DBO\Relation::init()
								->localKey('Categories')
								->reversedMember('Categories')
								->postLinkKey('id')
								->foreignKey('Pin')
								->foreignClass('\DBO\Pin\Category')
			);
			static::AddRelation($class,
								\DBO\Relation::init()
								->localKey('Keywords')
								->reversedMember('Keywords')
								->postLinkKey('id')
								->foreignKey('Pin')
								->foreignClass('\DBO\Pin\Keyword')
			);
			static::AddRelation($class,
								\DBO\Relation::init()
								->localKey('Levels')
								->reversedMember('Levels')
								->postLinkKey('id')
								->foreignKey('Pin')
								->foreignClass('\DBO\Pin\ActiveLevel')
			);
			if (false) // I don't remember how to inject lambda into this, look up how and make this only return levels that are active
				static::AddRelation($class,
									\DBO\Relation::init()
									->localKey('ActiveLevels')
									->reversedMember('ActiveLevels')
									->postLinkKey('id')
									->foreignKey('Pin')
									->foreignClass('\DBO\Pin\ActiveLevel')
				);
		}
	}

	public static function sort($a, $b) {
		if ($a instanceof self && $b instanceof self) {
			$aLevel = $a->Level();
			$bLevel = $b->Level();
			
			if ($aLevel != $bLevel) return ($aLevel < $bLevel) ? 1 : -1;
				
			$aName = $a->FieldValue('companyname');
			$bName = $b->FieldVAlue('companyname');

			if ($aName == $bName) return 0;
			return ($aName < $bName) ? -1 : 1;
		}
	}

	public function KeywordsChunk($from=0) {
		try {
			$search = \DBI\Search::init(\DBO\Pin\Keyword::TABLE);
			$search->join($k=\DBI\InnerJoin::init(\DBO\Keyword::TABLE)->on('id', \DBI\Condition::EQ, $search->Keyword))
				->where('Pin', \DBI\Condition::EQ, $this->id)
				->and($k->status, \DBI\Condition::EQ, 'Enabled')
				->offset($from)
				->limit(6);

			return \DBO\Pin\Keyword::fetchAll($search);

		} catch (\Exception $e) {
			return NULL;
		}
	}
	
    public function Level() {
		if (($al=$this->ActiveLevel())) return $al->Level();
		else return null;
	}
	/*
DROP FUNCTION PinActiveLevel;
DELIMITER $$
CREATE FUNCTION PinActiveLevel(pin BIGINT(20) UNSIGNED) RETURNS BIGINT(20) UNSIGNED
BEGIN
  DECLARE ret BIGINT(20) UNSIGNED;
  SET ret = (
    SELECT `Pin_ActiveLevel_1`.id
      FROM `Pin_ActiveLevel` as `Pin_ActiveLevel_1`
     WHERE `Pin_ActiveLevel_1`.`Pin`=pin
       AND (`Pin_ActiveLevel_1`.`start`>=NOW()
         OR `Pin_ActiveLevel_1`.`start` IS NULL )
       AND (`Pin_ActiveLevel_1`.`end`<=NOW()
         OR `Pin_ActiveLevel_1`.`end` IS NULL )
  ORDER BY `Pin_ActiveLevel_1`.`level` DESC
     LIMIT 1);
  RETURN ret;
END$$
DELIMITER ;

DROP FUNCTION PinLevel;
DELIMITER $$
CREATE FUNCTION PinLevel(pin BIGINT(20) UNSIGNED) RETURNS BIGINT(20) UNSIGNED
BEGIN
  DECLARE ret BIGINT(20) UNSIGNED;
  SET ret = (
    SELECT `PinLevel`.id
      FROM `Pin_ActiveLevel` as `Pin_ActiveLevel_1`
INNER JOIN `PinLevel`
        ON `PinLevel`.`id`=`Pin_ActiveLevel_1`.`Level`
     WHERE `Pin_ActiveLevel_1`.`Pin`=pin
       AND (`Pin_ActiveLevel_1`.`start`>=NOW()
         OR `Pin_ActiveLevel_1`.`start` IS NULL )
       AND (`Pin_ActiveLevel_1`.`end`<=NOW()
         OR `Pin_ActiveLevel_1`.`end` IS NULL )
  ORDER BY `Pin_ActiveLevel_1`.`level` DESC
     LIMIT 1);
  RETURN ret;
END$$
DELIMITER ;
	 */

	// Pin Level Accessor
    public function ActiveLevel() {
		try {
			$search = \DBI\Search::init(\DBO\Pin\ActiveLevel::TABLE)
				->where('Pin',\DBI\Condition::EQ,$this->id)
				->and(\DBI\ConditionGroup::init()->where('start',\DBI\Condition::LE,\DBI\MysqlFunction::NOW())->or('start',\DBI\Condition::ISNULL))
				->and(\DBI\ConditionGroup::init()->where('end',\DBI\Condition::GE,\DBI\MysqlFunction::NOW())->or('end',\DBI\Condition::ISNULL))
				->and('status', \DBI\Condition::EQ, 'Enabled')
				->orderBy('level',\DBI\OrderBy::DESC)
				->limit(1);

			return \DBO\Pin\ActiveLevel::fetchOne($search);
		} catch (\DBI\Exception $e) { return null; }
	}

	public function LevelHistory() {
		try {
			
			$search = \DBI\Search::init(\DBO\Pin\ActiveLevel::TABLE)
				->where('Pin', \DBI\Condition::EQ, $this->id)
				->orderBy('start', \DBI\OrderBy::ASC);

			return \DBO\Pin\ActiveLevel::fetchAll($search);

		} catch (\DBI\Exception $e) { return null; }
	}

	// Analytics
	public function fetchAnalytic() {
		$search = \DBI\Search::init(\DBO\Pin\Analytics::TABLE)
			->where('Pin', \DBI\Condition::EQ, $this->id)
			->limit(1);

		$out = NULL;
		try { 
			$out = \DBO\Pin\Analytics::fetchOne($search); 
			if (!$out instanceof \DBO\Pin\Analytics) throw new \DBI\Exception();
		} catch (\DBI\Exception $e) { $out = \DBO\Pin\Analytics::createObj(array('Pin' => $this->id)); }

		return $out;
	}
	
	// Field Accessor
	public function Field($name) {
		$field = \DBO\Field::fetch($name);
		if ($field==null) throw new \DBI\Exception('Failed to find field:'.$name);
		
		try {
			return \DBO\Pin\Field::fetchOne(\DBI\Search::init(\DBO\Pin\Field::TABLE)->where('Pin',\DBI\Condition::EQ,$this->id)->and('Field',\DBI\Condition::EQ,$field->id));
		} catch (\DBI\Exception $e) { return null; }
	}
	public function hasField($name) {
		if ($this->Field($name) instanceof \DBO\Pin\Field) return true;
		else return false;
	}
	public function FieldValue($name) {
		if (($field=$this->Field($name)) instanceof \DBO\Pin\Field) return $field->value;
		else return null;
	}

	function Category($offset=0) {
		$categories = $this->Categories();
		$categories->search()->limit(1)->offset($offset);
		
		if ($categories->count()) {
			foreach ($categories as $cat) {
				return $cat;
			}
		}

		return NULL;
	}

	// Fetch Pins for a given user
	// @TODO: This probably should be moved to the user object
	static public function UserPins($type=1, $user=null, $account=null, $limit=null, $offset=null, $name=null, $orderby=null, $direction='ASC') {
		if (is_null($user)) $user = \DBO\SessionUser::GetUser();
		if (is_numeric($user)) $user = (int)$user;
		else if ($user instanceof \DBO\User) $user = $user->id;
		else $user = false;
		if (!$user) throw new \DBO\Exception('Invalid user supplied');

		if (is_null($account)) $account = \DBO\Account::SessionAccount();
		if (is_numeric($account)) $account = (int)$account;
		else if ($account instanceof \DBO\Account) $account = $account->id;
		else if ($account!==FALSE) throw new \DBO\Exception('Invalid account supplied'); // Allow for account being FALSE, to return all user pins regardless of account. Otherwise, throw exception.
		
		$search = \DBI\Search::init(\DBO\Pin::TABLE)			
			->where('Owner',\DBI\Condition::EQ,$user)
			->and('Type',\DBI\Condition::EQ,$type)
			->and('status',\DBI\Condition::IN, array('Enabled', 'Disabled'));

		if ($name && strlen($name)) {
			$search->join($f=\DBI\InnerJoin::init(\DBO\Pin\Field::TABLE)->on('Pin', \DBI\Condition::EQ, $search->id)->and('Field', \DBI\Condition::IN, array(19,20)))
				->and($f->value, \DBI\Condition::_LIKE_, $name)
				->groupby($search->id);
		}

		if ($account) $search->and('Account',\DBI\Condition::EQ,$account);
		if ($limit) $search->limit($limit);
		if ($offset) $search->offset($offset);

		if (!is_null($orderby)) {
			switch (strtolower($direction)) {
			case 'desc':
				$direction = \DBI\OrderBy::DESC;
				break;

			default:
				$direction = \DBI\OrderBy::ASC;
			}

			switch (strtolower($orderby)) {
			case 'company':
				$search->join($cname=\DBI\InnerJoin::init(\DBO\Pin\Field::TABLE)->on('Pin', \DBI\Condition::EQ, $search->id)->and('Field', \DBI\Condition::EQ, 19))
					->orderBy($cname->value, $direction);
				break;
			case 'contact':
				$search->join($cname=\DBI\InnerJoin::init(\DBO\Pin\Field::TABLE)->on('Pin', \DBI\Condition::EQ, $search->id)->and('Field', \DBI\Condition::EQ, 20))
					->orderBy($cname->value, $direction);
				break;

			case 'date':
				$search->orderBy($search->start_time, $direction);
				break;
				
			case 'level':
				$search->join($cname=\DBI\InnerJoin::init(\DBO\Pin\ActiveLevel::TABLE)->on('Pin', \DBI\Condition::EQ, $search->id))
					->orderBy($cname->Level, $direction);
				break;

			}
		}
		
		//$search->debug();
		
		return \DBO\Pin::fetchAll($search);
	}

	// For Server Side Geocoding
	static public function geocode($string){
 
		$string = str_replace (" ", "+", urlencode($string));
		$details_url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$string."&sensor=false";
 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $details_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = json_decode(curl_exec($ch), true);
 
		// If Status Code is ZERO_RESULTS, OVER_QUERY_LIMIT, REQUEST_DENIED or INVALID_REQUEST
		if ($response['status'] != 'OK') {
			throw new \Exception($response['status']);
		}
 
		$geometry = $response['results'][0]['geometry'];
 
		$longitude = $geometry['location']['lng'];
		$latitude = $geometry['location']['lat'];
 
		$array = array(
			'latitude' => $latitude,
			'longitude' => $longitude,
			'location_type' => $geometry['location_type'],
		);
 
		return $array;
 
	}
	public function Duplicate($params=null) {
/*
| AnalyticsEvent_Pin           |
| Analytics_Pin                |
| PinType_Field                |
| Pin_ActiveLevel              |
| Pin_Keyword                  |
					   */
		$columns_Pin = array(
			'business_alliance' => null,
			'show_on_map' => null,
			'shop_local' => null,
			'DealImageID' => null,
			'LogoID' => null,
			'chamber' => null,
			'expire_time' => null,
			'start_time' => null,
			'event_date' => null,
			'home_business' => null,
			'pin_icon' => null,
			'zoom_max' => null,
			'zoom_min' => null,
			'AccountID' => null,
			'OwnerID' => null,
			'TypeID' => null,
			'AddressID' => null,
			'name' => null,
			'start' => null,
			'end' => null,
			'status' => null,
			'zoombits' => null,
		);
		$columns_PinFields = array(//'Pin' => null, // Never include this column. Also never include id, created, CreatedBy, modified, ModifiedBy, or timestamp.
			'FieldID' => null,
			'value' => null,
			'status' => null,
		);
		$columns_PinCategories = array(//'Pin' => null, // Never include this column. Also never include id, created, CreatedBy, modified, ModifiedBy, or timestamp.
			'CategoryID' => null,
			'status' => null,
		);
		
		$create = array();
		$create['Levels'] = array(0 => array('Level' => 1));
		foreach ($columns_Pin as $k => $v) {
			if (is_null($v)) $create[$k] = $this->$k;
			else throw new \DBO\Exception('Additional callback is not supported yet.');
		}
		
		if ($fields=$this->Fields()) {
			$create['Fields'] = array();
			foreach ($fields as $field) {
				$data = array();
				foreach ($columns_PinFields as $k => $v) {
					if (is_null($v)) $data[$k] = $field->$k;
					else throw new \DBO\Exception('Additional callback is not supported yet.');
				}
				$create['Fields'][] = $data;
			}
		}
		
		if ($categories=$this->Categories()) {
			$create['Categories'] = array();
			foreach ($categories as $category) {
				$data = array();
				foreach ($columns_PinCategories as $k => $v) {
					if (is_null($v))
						if (empty($category->$k)) $data[$k] = null;
						else $data[$k] = $category->$k;
					else throw new \DBO\Exception('Additional callback is not supported yet.');
				}
				$create['Categories'][] = $data;
			}
		}
		//print '<pre style="border: 3px solid orange;">'; print_r($create); print '</pre>';
		return \DBO\Pin::create($create);
	}

	static public function create(array $data, \DBO\Result $result=null) {
		if (isset($data['Keywords'])) {
			foreach ($data['Keywords'] as &$keyword) {
				if (isset($keyword['Keyword']) && is_array($keyword['Keyword'])) {
					$search = \DBI\Search::init(\DBO\Keyword::TABLE);
					$search->where('name', \DBI\Condition::LIKE, $keyword['Keyword']['name']);

					$kw = \DBO\Keyword::fetchOne($search);
					if ($kw instanceof \DBO\Keyword) 
						$keyword['Keyword'] = $kw->id;
				}
			}
		}

		if (isset($data['TwitterFeedWidgetCode'])) {
			$widgetCode = trim(consume('TwitterFeedWidgetCode', $data));

			// Execute Regex
			preg_match('/@([^<]+)/i', $widgetCode, $UNMatches);
			preg_match('/data-widget-id=.([\d]+)/i', $widgetCode, $IDMatches);

			// Add Field Creation to $data
			if (isset($UNMatches[1]) && isset($IDMatches[1])) {
				$data['Fields'][] = array(
					'Field' => 47,
					'value' => $UNMatches[1]
				);
				$data['Fields'][] = array(
					'Field' => 48,
					'value' => $IDMatches[1]
				);
			}
		}

		$result = parent::create($data, $result);
		if (($pin=$result->getObject()) instanceof \DBO\Pin) {
			// Send Notification
			self::sendNotificationEmail($pin);

			// Update Stored Pin Count
			foreach ($pin->Categories() as $pinCategory) {
				if (($cat=$pinCategory->Category()) instanceof \DBO\Category)
					$cat->updateStoredPinCount();
			}
		}
		return $result;
	}

	public function update($data, \DBI\Update $update=null, \DBO\Result $result=null) {
		if (isset($data['Keywords'])) {
			foreach ($data['Keywords'] as &$keyword) {
				if (isset($keyword['Keyword']) && is_array($keyword['Keyword'])) {
					$search = \DBI\Search::init(\DBO\Keyword::TABLE);
					$search->where('name', \DBI\Condition::LIKE, $keyword['Keyword']['name']);

					$kw = \DBO\Keyword::fetchOne($search);
					if ($kw instanceof \DBO\Keyword) 
						$keyword['Keyword'] = $kw->id;
				}
			}
		}

		if (isset($data['TwitterFeedWidgetCode'])) {
			$widgetCode = trim(consume('TwitterFeedWidgetCode', $data));

			// Execute Regex
			preg_match('/@([^<]+)/i', $widgetCode, $UNMatches);
			preg_match('/data-widget-id=.([\d]+)/i', $widgetCode, $IDMatches);

			// Modify/Create as needed
			if (isset($UNMatches[1]) && isset($IDMatches[1])) {
				if ($this->hasField(47)) $this->Field(47)->update(array('value' => $UNMatches[1]));
				else \DBO\Pin\Field::create(array('Pin' => $this->id, 'Field' => 47, 'value' => $UNMatches[1]));

				if ($this->hasField(48)) $this->Field(48)->update(array('value' => $IDMatches[1]));
				else \DBO\Pin\Field::create(array('Pin' => $this->id, 'Field' => 48, 'value' => $IDMatches[1]));
			}
		}

        if (isset($data['home_business']) && $data['home_business'] == 0) {
            $data['show_on_map'] = 1;
        }


		$result = parent::update($data, $update, $result);

		// Send Notification
		self::sendNotificationEmail($this, 'MapIt ' . \DBO\Account::SessionAccount()->name . ' ' . ($this->status == 'Enabled' ? 'Updated' : 'Deleted') . ' ' . $this->Type()->name . ' Level ' . $this->Level()->id . ' Pin');

		// Update Stored Pin Count
		foreach ($this->Categories() as $pinCategory) {
			if (($cat=$pinCategory->Category()) instanceof \DBO\Category)
				$cat->updateStoredPinCount();
		}

		return $result;
	}

	static public function sendNotificationEmail(\DBO\Pin $pin, $subject=null) {
		try {
			$account = \DBO\Account::SessionAccount();
			$user = \SessionUser::getUser();

			// Validate Pin Info
			if (!$pin instanceof \DBO\Pin) throw new \Exception();
			if (!$pin->Type() instanceof \DBO\Pin\Type) throw new \Exception();
			if (!$pin->Level() instanceof \DBO\Pin\Level) throw new \Exception();
			if (!$user instanceof \DBO\User) throw new \Exception();
			if (!$account instanceof \DBO\Account) throw new \Exception();

			$mail = new \Mailer();

			// Email Info
			$mail->setFrom('info@mapitusa.com', 'MapItUSA');
			$mail->addReplyTo('info@mapitusa.com', 'MapItUSA');
			$mail->addAddress('info@mapitusa.com');

			// Add the active user to the email
			if ($user->Person()->email) $mail->addAddress($user->Person()->email);

			// Add the pin owner to the email (if not the active user)
			if ($pin->Owner() instanceof \DBO\User && $user->id != $pin->Owner()->id && $pin->Owner()->Person() instanceof \DBO\Person && $pin->Owner()->Person()->email) $mail->addAddress($pin->Owner()->Person()->email);

			// Add Account specific notifications
			$string = $account->TryConfigValue('mapit.point.'.strtolower($pin->Type()->name).'.notification');
			if ($string) {
				foreach (explode(',', trim($string)) as $email) {
					$mail->addAddress($email);
				}
			}

			// Setup Email Subject & Content
			$mail->Subject = is_null($subject) ? 'MapIt ' . $account->name . ' New ' . $pin->Type()->name . ' Level ' . $pin->Level()->id . ' Pin' : $subject;
			$mail->Body    = '--- Level 1 Info ---' . "\n";
			$mail->Body   .= 'Field Data:' . "\n";
			$mail->Body   .= 'Company Name: ' . $pin->FieldValue('companyname') . "\n";
			$mail->Body   .= 'Contact Name: ' . $pin->FieldValue('contactname') . "\n";
			$mail->Body   .= 'Phone 1: ' . $pin->FieldValue('phone1') . "\n";
			$mail->Body   .= 'Phone 2: ' . $pin->FieldValue('phone2') . "\n";
			$mail->Body   .= 'Fax: ' . $pin->FieldValue('fax') . "\n";
			$mail->Body   .= 'Email: ' . $pin->FieldValue('email') . "\n";
			if (($address=$pin->Address()) instanceof \DBO\Address) {
				$mail->Body .= 'Address 1: ' . $address->line1 . "\n";
				$mail->Body .= 'Address 2: ' . $address->line2 . "\n";
				$mail->Body .= 'City: ' . $address->city . "\n";
				$mail->Body .= 'State: ' . $address->state . "\n";
				$mail->Body .= 'Zip: ' . $address->zip . "\n";
			} else {
				$mail->Body .= 'Address 1: ' . "\n";
				$mail->Body .= 'Address 2: ' . "\n";
				$mail->Body .= 'City: ' . "\n";
				$mail->Body .= 'State: ' . "\n";
				$mail->Body .= 'Zip: ' . "\n";
			}
			$mail->Body   .= 'Level: ' . $pin->Level()->id . "\n";
			$mail->Body   .= 'Type: ' . $pin->Type()->name . "\n";
			$mail->Body   .= 'Status: ' . $pin->status . "\n";

			$mail->Body   .= "\n" . '--- Categories ---' . "\n";
			foreach ($pin->Categories() as $pinCategory) {
				if (($category=$pinCategory->Category()) instanceof \DBO\Category) {
					$mail->Body .= $category->name . "\n";
				}
			}

			$keywords = $pin->Keywords();
			if (count($keywords)) {				
				$mail->Body .= "\n" . '--- Keywords ---' . "\n";
				foreach ($keywords as $pinKeyword) {
					if (($keyword=$pinKeyword->Keyword()) instanceof \DBO\Keyword) {
						$mail->Body .= $keyword->name;
					}
				}
			}

			if ($pin->Level()->id >= 2) {
				$mail->Body .= "\n" . '--- Level 2 Info ---' . "\n";
				$mail->Body .= 'Website: ' . $pin->FieldValue('website') . "\n";
				$mail->Body .= 'Twitter: ' . $pin->FieldValue('twitter') . "\n";
				$mail->Body .= 'Facebook: ' . $pin->FieldValue('facebook') . "\n";
				$mail->Body .= 'Google+: ' . $pin->FieldValue('googleplus') . "\n";
				$mail->Body .= 'LinkedIn: ' . $pin->FieldValue('linkedin') . "\n";
				$mail->Body .= 'Pinterest: ' . $pin->FieldValue('pinterest') . "\n";
				$mail->Body .= 'YouTube: ' . $pin->FieldValue('youtube') . "\n";
				$mail->Body .= 'Has Logo Image? ' . ($pin->Logo() instanceof \DBO\File ? 'Yes' : 'No') . "\n";
			}

			if ($pin->Level()->id >= 3) {
				$mail->Body .= "\n" . '--- Level 3 Info ---' . "\n";
				$mail->Body .= 'Facebook Feed: ' . $pin->FieldValue('Facebook Feed') . "\n";
				$mail->Body .= 'Has Deal? ' . ($pin->DealImage() instanceof \DBO\File ? 'Yes' : 'No') . "\n";				
			}			

			if ($pin->Level()->id >= 4) {
				$mail->Body .= "\n" . '--- Level 4 Info ---' . "\n";
				$mail->Body .= 'Video URL: ' . $pin->FieldValue('youtubevideo');
			}
			
			if (!$mail->send()) throw new \Exception($mail->ErrorInfo);
		} catch (\Exception $e) {
			//echo 'Something went boom: ' . $e->getMessage();
		}		
	}

}