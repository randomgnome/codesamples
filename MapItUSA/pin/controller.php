<?php namespace modules\point;

class Controller extends \Controller {
	protected $default_data = array();
	protected $name = 'point';

	public function __construct(&$params) {
		parent::__construct($param);
		if (isset($params['id']) && is_numeric($params['id'])) {
			try {
				$this->node = \DBO\Pin::fetch($params['id']);
			} catch (\Exception $e) {
				$this->node = NULL;
			}
		}
		$this->processContext(consume('context',$params), $params);
	}

	protected function context(\DeguArrayIterator $iter, &$params = NULL) {
		switch (strtolower($iter->current())) {
		case 'pin':
			try {
				$json = array('success' => true);
				switch (strtolower($iter->nextValue())) {
				case 'upgrade':
					if (!$this->node instanceof \DBO\Pin) throw new \Exception('Invalid Pin ID');
					if (($level = consume('level', $params, false)) === FALSE) throw new \Exception('No Level Requested');
					if (!($level = \DBO\Pin\Level::fetch($level)) instanceof \DBO\Pin\Level) throw new \Exception('Requested Level is Invalid');

					if (!$this->node->OwnerID) throw new \Exception('Pin has no defined owner, so we cannot create an invoice to process your upgrade. Please contact support');

					$invoice = array(
						'PaymentGateway' => 1,
						'Account' => $this->node->Account(),
						'Owner' => $this->node->OwnerID,
						'Items' => array(
							array(
								'Type'   => 3,
								'cost'   => $this->node->Account()->TryConfigValue('mapit.point.' . strtolower($this->node->Type()->name) . '.level-' . $level->id . '.cost'),
								'params' => json_encode(
									array(
										'pin' => $this->node->id,
										'level' => $level->id,
										'plan' => $this->node->Account()->TryConfigValue('mapit.point.' . strtolower($this->node->Type()->name) . '.level-' . $level->id . '.plan')
									)
								)
							)
						)
					);

					// Redirect to the payment page
					$invoice = \DBO\Invoice::createObj($invoice);
					$json['redirect'] = '/m/invoice/payment/' . $invoice->id;
					break;

				case 'duplicate':
					if ($this->node instanceof \DBO\Pin) {
						$result = $this->node->Duplicate();
						if (!$result->getObject() instanceof \DBO\Pin) throw new \Exception('Could not duplicate pin due to an internal error');
						$json['id'] = $result->getObject()->id;
					}
					break;

				case 'create':
					// @TODO: I'm sure consume could make this much simpiler, too tired to mess with it right now
					if (isset($_REQUEST['process']) && isset($_REQUEST['process']['DBO-Pin']) && isset($_REQUEST['process']['DBO-Pin']['create']) && isset($_REQUEST['process']['DBO-Pin']['create'][0])) {
						$pinData = $_REQUEST['process']['DBO-Pin']['create'][0];

						// For some reason if you have non-sequential indexies for the fields some won't be made, so we will use array_values to make sure the indexies are sequential
						// @TODO: Consider moving this into \DBO\Pin::create
						if (isset($pinData['Fields']) && count($pinData['Fields'])) $pinData['Fields'] = array_values($pinData['Fields']);

						// Validate that we have the bare minimum data to move forward
						// Needed: Level, Type, Account, Owner
						if (!isset($pinData['Levels']) || !isset($pinData['Type']) || !isset($pinData['Owner']) || !isset($pinData['Account'])) throw new \Exception('Missing required information. Cannot process pin create');
						
						try {
							$type  = \DBO\Pin\Type::fetch((int)$pinData['Type']);
							$owner = \DBO\User::fetch((int)$pinData['Owner']);
							$account = \DBO\Account::fetch((int)$pinData['Account']);
							$level   = \DBO\Pin\Level::fetch((int)$pinData['Levels'][0]['Level']);
						} catch (\Exception $e) {
							throw new \Exception('Could not process point data');
						}

						// Does this pin require payment?
						$price = $account->TryConfigValue('mapit.point.' . strtolower($type->name) . '.level-' . $level->id . '.cost', null, $owner);

						if ($price <= 0) {
							\DBO\Pin::create($pinData);
							$json['redirect'] = '/m/panel/backend/dashboard?message='. urlencode('Pin sucesfully created') . '&messageType=success';
						} else {
							// Yes, it does. So lets make an invoice!
							$invoice = array(
								'PaymentGateway' => 1,
								'Account' => $account->id,
								'Owner' => $owner->id,
								'Items' => array(
									array(
										'Type' => 1,
										'cost' => $price,
										'params' => json_encode(array('pinData' => $pinData, 'plan' => $account->TryConfigValue('mapit.point.' . strtolower($type->name) . '.level-' . $level->id . '.plan', null, $owner)))
									)
								)
							);

							// Redirect to the payment page
							$invoice = \DBO\Invoice::createObj($invoice);
							$json['redirect'] = '/m/invoice/payment/' . $invoice->id;
						}

					}
					break;
				}
			} catch (\Exception $e) {
				$json = array('success' => false, 'error' => true, 'message' => $e->getMessage());
			}
			die(json_encode($json));
			break;
		}
	}
}